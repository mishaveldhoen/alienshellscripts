#!/bin/bash
# Kills all masterjobs with a PID between minPID and maxPID. 
# The script will first show a list of masterjobs that will be killed, and then asks
# for confirmation.

# Usage: 
# Start an aliensh session,
# type: "run killRange.sh minPID maxPID"

# Setting the minPID to the first argument or to 0. Note that usually $0 is the script name,
# and $1 is the first argument. Somehow when using the aliensh, $1 is the script name, and $2 is
# the first argument.

NOCOLOR="\e[0m"
RED="\e[0;31m"
GREEN="\e[0;32m"
YELLOW="\e[;33m"
BLUE="\e[0;34m"
PURPLE="\e[0;35m"

minPID=$2
if [ $# -le 1 ] 
	then
	minPID=1
fi

# Setting the maxPID to the second argument or to 999999999 
maxPID=$3
if [ $# -le 2 ]
	then
	maxPID=999999999
fi

# Checking if the range is OK.
if [ ! "$minPID" -le "$maxPID" ]
	then 
	printf "ERROR: maxPID must be greater than or equal to minPID.\n"
	exit 0
fi  

# Printing status.
printf "Resubmitting failed subjobs of masterjobs in the range: %s <= PID <= %s.\n" "$minPID" "$maxPID"

# Print out a list of masterjobs that will be looped over.
nSubJobs=0
masterJobTmp=0
for masterJob in `gbbox ps -A | awk '{print $2}'`
do 
	if [ "$masterJob" -le "$maxPID" -a "$masterJob" -ge "$minPID" ]
		then

		# Return if there's a big gap in the PID's, suggesting separate submissions.
		if [ "$masterJobTmp" -le "$masterJob" ] 
			then
			printf "\n"
		fi

		# Print the job PID.
		(( nSubJobs += 1 ))
		printf "Number: %s, PID: %s | " "$nSubJobs" "$masterJob"

		# Print subjob statusses.
		nextIsANumber=0
		for subjobStatus in `masterjob $masterJob | grep "Subjobs" | awk '{print $3, $4}'`
		do
			# Determine the color of the subjob status
			if [ $nextIsANumber -eq 0 ]; then

				nextIsANumber=1

				if [ $subjobStatus == "DONE:" ]; then
					printf "$GREEN"
				elif [ $subjobStatus == "RUNNING:" ]; then
					printf "$BLUE"
				elif [ $subjobStatus == "WAITING:" ]; then
					printf "$PURPLE"
				elif [ ${subjobStatus:0:5} == "ERROR" ]; then
					printf "$RED"
				else 
					# Default color when status is not recognized.
					printf "$NOCOLOR"
				fi
			else 
				nextIsANumber=0
			fi

			# Reset color code
			printf "%s" "$subjobStatus "
			
		done
		printf "$NOCOLOR\n"

		# Reset masterJobTmp value, to be the current PID plus 100.
		let "masterJobTmp = masterJob + 100"

	fi
done

# Ask for confirmation to continue.
read -p "Do you want to proceed killing these masterjobs? [y/n]? " -n 1 -r
echo

# Resubmit subjobs when confirmed.
if [ $REPLY == "Y" -o $REPLY == "y" ] 
	then
	for masterJob in `gbbox ps -A | awk '{print $2}'`
	do 
		if [ "$masterJob" -le "$maxPID" -a "$masterJob" -ge "$minPID" ]
			then
			kill $masterJob
		fi
	done
fi
